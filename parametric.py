#  Regular temperament searches using parametric badness.

from __future__ import division

#
#  definitions
#

import math, regutils

#
#  Equal Temperaments
#

def uvSurvey1(uvs, nResults=10, plimit=None):
    """Returns equal temperaments that temper out the given unison vectors"""
    if plimit is None:
        uvs, plimit = regutils.setBestLimit(uvs)
    def desired(et):
        for uv in uvs:
            if not regutils.tempersOut(et, uv):
                return False
        return True
    return getEqualTemperaments(plimit,
            ek_for_search(uvs, plimit),
            nResults=nResults,
            valid=desired,
            ttl=40)

def getEqualTemperaments(plimit, ek, nResults=10, valid=None, ttl=50):
    """Returns the best nResults equal temperament mappings
    within the given prime limit
    scored according to the parameter ek
    and filtered according to the criterion valid.
    The time to live (ttl) determines how long to try to find the badness
    """
    # stop weird things happening for non-standard units
    plimit = [p/plimit[0] for p in plimit]

    def mapping_badness(mapping):
        tc = TemperamentClass(plimit, [mapping])
        return tc.optimalBadness(ek)

    # Start with a reasonable guess guaranteed to be high enough
    # where a "valid" filter isn't used.
    results = regutils.ConservativeList(nResults)
    for nNotes in range(1, nResults + len(plimit)):
        pmap = regutils.prime_mapping(nNotes, plimit)
        tc = TemperamentClass(plimit, [pmap])
        results.add(tc, tc.optimalBadness(ek))
    bmax = results.badnessCap()

    for each in range(ttl):
        nNotes = 1
        results = regutils.ConservativeList(nResults)
        while nNotes <= bmax/ek:
            mappings = limitedMappings(nNotes, ek, bmax, plimit)
            for mapping in mappings:
                if valid is None or valid(mapping):
                    tc = TemperamentClass(plimit, [mapping])
                    results.add(tc, tc.optimalBadness(ek))
                    if results.full():
                        bmax = results.badnessCap()
            nNotes += 1

        if results.full():
            break

        # The initial bmax cap should be correct unless there's a filter
        assert valid, "Wanted {}, got {}".format(
                                nResults, len(results.queue))

        bmax *= 1.1

    return [result.melody[0] for result in results.retrieve()]

def bestET(plimit, nNotes):
    pmap = regutils.prime_mapping(nNotes, plimit)
    tc = TemperamentClass(plimit, [pmap])
    bmax = tc.optimalBadness(0) * 1.01
    ets = sorted_by_badness(
            limitedMappings(nNotes, 0, bmax, plimit), plimit, 0)
    return ets[0]

def limitedMappings(nNotes, ek, bmax, plimit):
    """Return all ET mappings with the given number of notes and the
    square parametric badness less than that given.
    """
    cap = bmax**2 * len(plimit)/plimit[0]**2
    # convert from ek form to epsilon squared form
    epsilon2 = ek**2/(1 + ek**2)

    def moreLimitedMappings(mapping, tot, tot2):
        """mapping - the ET mapping with a new entry
        tot - running total of w
        tot2 - running total of w squared
        """
        i = len(mapping)
        weightedSize = mapping[-1]/plimit[i-1]
        tot += weightedSize
        tot2 += weightedSize**2
        lamda = 1 - epsilon2
        # error test for i instead of i+1
        assert regutils.lae(tot2, lamda*tot**2/i + cap), "%r > %r"%(
                tot2 - lamda*tot**2/i, cap)
        if i==len(plimit):
            # recursion stops here
            yield mapping
        else:
            toti = tot*lamda/(i + epsilon2)
            error2 = tot2 - tot*toti
            if error2 < cap:
                target = plimit[i]
                deficit = math.sqrt((i+1)*(cap-error2)/(i + epsilon2))
                xmin = target*(toti - deficit)
                xmax = target*(toti + deficit)
                for guess in intrange(xmin, xmax):
                    results = moreLimitedMappings(mapping+[guess], tot, tot2)
                    for result in results:
                        yield result

    return moreLimitedMappings([nNotes], 0.0, 0.0)

def etsForMapping(mapping, plimit=None, ek=None, nResults=None, octave=None):
    if len(mapping) == 1:
        # trivial case
        return [list(mapping[0])]
    assert mapping
    if plimit is None:
        plimit = regutils.primes[:len(mapping[0])]
    rank = len(mapping)
    assert rank == regutils.rank(mapping)
    key = regutils.lattice_invariant(mapping, rank)
    def in_mapping(etmap):
        mapplus = list(mapping) + [etmap]
        if regutils.rank(mapplus) == rank:
            try:
                return key == regutils.lattice_invariant(mapplus, rank)
            except AssertionError:
                return False
    if octave is None:
        if nResults is None:
            nResults = rank
        if ek is None:
            ek = 8e-4
        return getEqualTemperaments(plimit, ek, nResults, in_mapping)
    if nResults is None:
        nResults = 1
    if ek is None:
        ek = 0.5/max(plimit)/octave
    bmax = ek * octave
    for loop in range(20):
        etgen = limitedMappings(octave, ek, bmax, plimit)
        ets = []
        try:
            for check in range(1000):
                et = next(etgen)
                if in_mapping(et):
                    ets.append(et)
            if len(ets) < nResults:
                raise ValueError("This ET for this mapping is too obscure")
        except StopIteration:
            pass
        if len(ets) >= nResults:
            return sorted_by_badness(ets, plimit, ek)[:nResults]
        bmax *= 1.5

def sorted_by_badness(ets, plimit, ek):
    def badness(et):
        rt = TemperamentClass(plimit, [et])
        return rt.optimalBadness(ek)

    return sorted(ets, key=badness)

def ek_for_search(uvs, plimit):
    """Choose a value for the parameter ek
    that should work with the given unison vectors
    in the given prime limit.
    """
    return max([regutils.inherentError(uv, plimit) for uv in uvs])

#
# Finding ETs belonging to a temperament class
# (requires tuning from outside this module)
#

def etsForTemperament(rt, ek=None, nResults=10):
    """Return equal temperaments consistent with the given regular temperament.
    The regular temperament object is like TemperamentClass
    but with a .basis member holding the tuning in octaves
    and .primes as an alternative to .plimit.
    """
    if ek is None:
        ek = rt.optimalError()
    try:
        plimit = rt.plimit # for a souped up TemperamentClass
    except AttributeError:
        plimit = rt.primes # for a RegularTemperament from regular.py
    mapping = rt.melody
    transmap = list(zip(*mapping))

    rank, dimension = len(mapping), len(mapping[0])
    assert rank == regutils.rank(mapping)
    M = weightMapping(mapping, plimit)
    K = gramMatrix(M)
    E = gramMatrix(transmult([[1]*dimension], M))
    G = [[((1+ek**2)*k - e/dimension)/dimension
        for (k, e) in zip(krow, erow)]
        for (krow, erow) in zip(K, E)]
    bmax = rt.optimalBadness(ek) ** (1/rank) * 1.5

    if bmax/ek > 100000:
        raise SanityViolation("This is going to get silly")

    def badness(et):
        return bracket(et, G, et)

    for loop in range(10):
        results = regutils.ConservativeList(nResults)
        octave = 1
        while octave < bmax/ek:
            for et in quadraticETs(
                    octave, transmap[0], G, rt.basis, bmax**2):
                mapping = transmult(transmap, [et])[0]
                tc = TemperamentClass(plimit, [mapping])
                results.add(tc, tc.optimalBadness(ek))
                if results.full():
                    bmax = results.badnessCap()
            octave += 1

        if results.full():
            return [result.melody[0] for result in results.retrieve()]

        bmax *= 1.2

def quadraticETs(octave, octavemap, metric, JI, maxval):
    """Return all vectors (as lists) of integers
       below maxval according to the quadratic form defined by metric
       based on octave divisions of the interval defined by octavemap.
       JI is the ideal tuning used to guess the equal tunings.
    """
    rank = len(metric)
    assert len(JI) == rank
    def temperRow(i, tempered):
        if i==rank:
            if any(tempered):
                if regutils.dotprod(tempered, octavemap) == octave:
                    assert bracket(tempered, metric, tempered) <= maxval
                    yield tempered
            return
        if not any(octavemap[:i] + octavemap[i+1:]):
            # This generator is the period
            octavediv = octavemap[i]
            if octave % octavediv != 0:
                return # nothing to do here
            minx = maxx = octave//octavediv
        else:
            assert i < rank
            pivot = [0]*rank
            pivot[i] = 1
            others = list(tempered)
            others[i] = 0
            cap = maxval
            # Set parameters for quadratic function Ax2 + Bx + C
            A = metric[i][i]
            B = bracket(others, metric, pivot) + bracket(pivot, metric, others)
            C = bracket(others, metric, others) - cap
            assert A > 0 # no minimum otherwise
            discriminant = B**2 - 4*A*C
            if discriminant < 0:
                return
            middle = -B/2/A
            diff = math.sqrt(discriminant)/2/A
            # This range assumes all other maps are at their optimal values.
            # However, they will adapt if an early map is off-center,
            # so add some fudge.
            diff *= rank - i
            minx, maxx = middle - diff, middle + diff
        for newx in intrange(minx, maxx):
            guess = list(tempered)
            guess[i] = newx
            for mapping in temperRow(i+1, guess):
                yield mapping
    guess = [octave * x / regutils.dotprod(octavemap, JI) for x in JI]
    return list(temperRow(0, guess))

def getEqualTemperaments2(plimit, ek, nResults=10):
    """Duplicates getEqualTemperaments using JI as a temperament"""
    I = [[int(i==j) for i in range(len(plimit))] for j in range(len(plimit))]
    rt = TemperamentClass(plimit, I)
    rt.basis = plimit
    return etsForTemperament(rt, ek, nResults)

def gramMatrix(vectors):
    """Multiply a matrix by its transpose"""
    return transmult(vectors, vectors)

def bracket(X, G, Y):
    """Only element of the matrix product XGY
    with X as a row vector and Y as a column vector.
    """
    return transmult(transmult(G, [Y]), [X])[0][0]

def transmult(X, Y):
    """Matrix multiplication of X by the transpose of Y"""
    return [[regutils.dotprod(u, v) for u in X] for v in Y]

def weightMapping(vectors, buoyancies):
    return [[x/b for (x, b) in zip(v, buoyancies)] for v in vectors]

def intrange(x, y):
    # integers between x and y
    return range(int(math.ceil(x)), int(math.floor(y))+1)

#
#  Rank 2 temperament classes constructed from equal temperaments
#

def survey2(plimit, ek=8e-4, nResults=10, safety=40):
    ets = getEqualTemperaments(plimit, ek, nResults+safety)
    return getR2Mappings(plimit, ets, ek, nResults)

def uvSurvey2(uvs, nResults=10, safety=10, plimit=None):
    if plimit is None:
        uvs, plimit = regutils.setBestLimit(uvs)
    ets = uvSurvey1(uvs, nResults+safety, plimit)
    ek = ek_for_search(uvs, plimit)
    return getR2Mappings(plimit, ets, ek, nResults)

def getR2Mappings(plimit, ets, ek=8e-4, nResults=10):
    ets = list(ets) # in case it's a generator
    results = regutils.ConservativeList(nResults)
    for j in range(1, len(ets)):
        et2 = ets[j]
        for i in range(j):
            et1 = ets[i]
            lt = TemperamentClass(plimit, [et1, et2])
            if not lt.degenerate():
                badness = lt.optimalBadness(ek)
                if badness>0.0 and results.goodEnough(badness):
                    results.add(lt, badness)
    return results.retrieve()

#
#  Higher rank temperament classes constructed from lower ranks
#

def Temperament(plimit, *octaves):
    if isinstance(plimit, str):
        nums, plimit = regutils.textToLimit(plimit)
    return TemperamentClass(plimit, [bestET(plimit, n) for n in octaves])

temperament = Temperament

def survey3(plimit, ek=8e-4, nResults=10, safety=40):
    return survey(3, plimit, ek, nResults, safety)

def survey(r, plimit, ek=8e-4, nResults=10, safety=40):
    assert r > 2
    ets = getEqualTemperaments(plimit, ek, nResults+safety)
    rts = getR2Mappings(plimit, ets, ek, nResults)
    for rank in range(2, r):
        rts = higherRankSearch(plimit, ets, rts, ek, nResults)
    return rts

def uvSurvey3(uvs, nResults=10, safety=10, plimit=None):
    return uvSurvey(3, uvs, nResults, safety, plimit)

def uvSurvey(r, uvs, nResults=10, safety=10, plimit=None):
    assert r > 2
    if plimit is None:
        uvs, plimit = regutils.setBestLimit(uvs)
    ets = uvSurvey1(uvs, nResults+safety, plimit)
    ek = ek_for_search(uvs, plimit)
    rts = getR2Mappings(plimit, ets, ek, nResults)
    for rank in range(2, r):
        rts = higherRankSearch(plimit, ets, rts, ek, nResults)
    return rts

def higherRankSearch(plimit, ets, rts, ek=8e-4, nResults=10):
    # written as rank 3, but goes higher
    ets, r2s = list(ets), list(rts) # in case they're generators
    results = regutils.ConservativeList(nResults)
    for r22 in r2s:
        for et1 in ets:
            rt = TemperamentClass(plimit, [et1] + r22.melody)
            if not rt.degenerate():
                badness = rt.optimalBadness(ek)
                if badness>0.0 and results.goodEnough(badness):
                    results.add(rt, badness)
    return results.retrieve()

def fullSurvey(uvs, plimit=None, ek=None, nResults=10, safety=10):
    """Return results from unison vector searches at different ranks"""
    if plimit is None:
        uvs, plimit = regutils.setBestLimit(uvs)
    highestRank = len(plimit) - regutils.rank(uvs)
    if highestRank < 1:
        return [],
    if ek is None:
        ek = ek_for_search(uvs, plimit)
    def desired(et):
        for uv in uvs:
            if not regutils.tempersOut(et, uv):
                return False
        return True
    if highestRank==1:
        ets = getEqualTemperaments(plimit, ek, 1, valid=desired)
        return ets,
    ets = getEqualTemperaments(
            plimit, ek, nResults + safety, valid=desired, ttl=40)
    rts = getR2Mappings(plimit, ets, ek, nResults)
    if highestRank == 2:
        return ets[:nResults], rts[:1]
    results = ets[:nResults], rts[:nResults]
    # any exceptions to suppress from here?
    for rank in range(3, highestRank):
        rts = higherRankSearch(plimit, ets, rts, ek, nResults)
        results += rts[:nResults],
    return results + (higherRankSearch(plimit, ets, rts, ek, 1),)

def subSurvey(rt, ek=None, nResults=10, safety=10):
    """Search lower ranks for followers of the given temperament"""
    if ek is None:
        ek = rt.optimalError()
    try:
        plimit = rt.plimit # for a souped up TemperamentClass
    except AttributeError:
        plimit = rt.primes # for a RegularTemperament from regular.py
    highestRank = len(rt.melody)
    if highestRank==1:
        return [rt.melody]
    try:
        ets = etsForTemperament(rt, ek, nResults + safety) or []
    except SanityViolation:
        # ET search looks to large, return the case
        # with everything tempered out
        return [[]] * (highestRank - 1) + [[rt]]
    # Ensure the original result comes back out
    if regutils.rank(ets) < regutils.rank(rt.melody):
        for originalet in rt.melody:
            if regutils.rank(ets) < regutils.rank(ets + [originalet]):
                ets.append(originalet)
    rts = getR2Mappings(plimit, ets, ek, nResults)
    if highestRank == 2:
        return ets[:nResults], rts[:1]
    results = ets[:nResults], rts[:nResults]
    # any exceptions to suppress from here?
    for rank in range(3, highestRank):
        rts = higherRankSearch(plimit, ets, rts, ek, nResults)
        results += rts[:nResults],
    return results + (higherRankSearch(plimit, ets, rts, ek, 1),)

def superSurvey(rt, ek=None, nResults=10, safety=10):
    """Like a subSurvey but for higher ranks"""
    if ek is None:
        ek = rt.optimalError()
    try:
        plimit = rt.plimit # for a souped up TemperamentClass
    except AttributeError:
        plimit = rt.primes # for a RegularTemperament from regular.py
    safe_results = nResults + safety
    mapping = rt.melody
    initial_rank = len(mapping)
    ji_rank = len(mapping[0])
    def distinct_et(et_mapping):
        """Filters out contorted matches as well as subsets"""
        return len(mapping) != regutils.rank(mapping + [et_mapping])
    ets = getEqualTemperaments(plimit, ek, safe_results, distinct_et)
    rts = [rt]
    for start_rank in range(initial_rank, ji_rank - 1):
        rts = higherRankSearch(plimit, ets, rts, ek, safe_results)
        yield rts[:nResults]


#
#  The regular temperament class object
#

class TemperamentClass:
    """Regular temperament class of arbitrary rank.
    """

    def __init__(self, plimit, melody):
        """TemperamentClass constructor.
        plimit --  A list of the sizes of prime intervals in octaves.
        melody -- The mapping of prime intervals to scale steps.
                  A list if n tuples for a rank n temperament.
        """
        self.melody = list(melody)
        for mapping in self.melody:
            assert len(mapping) == len(plimit)
        self.plimit = plimit

    def invariant(self):
        """A tuple to uniquely identify the temperament class"""
        return regutils.lattice_invariant(self.melody)

    def complexity(self):
        """Scalar complexity"""
        return regutils.size_of_matrix(self.weightedMapping())

    def optimalBadness(self, ek=0.0):
        """Cangwu badness (a.k.a scalar badness)"""
        scaling = 1 - ek/math.sqrt(1 + ek**2)
        M = []
        for row in self.weightedMapping():
            rowmean = scaling*regutils.mean(row)
            M.append([m - rowmean for m in row])
        return regutils.size_of_matrix(M)

    def exteriorBadness(self):
        """TE/scalar badness using exterior-like algorithm"""
        M = self.weightedMapping()
        M.append([1]*len(M[0]))
        return regutils.size_of_matrix(M)

    def degenerate(self):
        """Check if the ETs are linearly dependent"""
        reduced = regutils.lattice_reduction(self.melody)
        return reduced[-1] == [0]*len(self.plimit)

    def optimalError(self):
        """TOP-RMS error"""
        return self.optimalBadness()/self.complexity()

    def weightedMapping(self):
        return [[x/p for x, p in zip(m, self.plimit)] for m in self.melody]

    def name(self):
        """Returns the name of the temperament class,
        and the number of prime dimensions below that it's
        properly defined in.
        """
        try:
            name, extras = regutils.nameThatMapping(self.melody)
            return name, extras
        except KeyError:
            sizes = [mel[0] for mel in self.melody]
            sizes.sort()
            return " & ".join(map(str, sizes)), 0

    def betterMelody(self):
        rank = len(self.melody)
        ek = self.optimalError() or 1e-7
        for dupes in range(10):
            ets = etsForMapping(self.melody,
                                self.plimit,
                                ek*(dupes+1),
                                rank+dupes)
            melody = []
            for etmap in ets:
                melody.append(etmap)
                if len(melody) > regutils.rank(melody):
                   del melody[-1]
            if len(melody) == rank:
                self.melody = melody
                return

    def __repr__(self):
        """basic stringification"""
        name, extras = self.name()
        return name.replace(u"\xfc", "ue") + '+'*extras

    def __str__(self):
        """The standard way of converting the temperament to a string.
        This is supposed to be readable, but still brief.
        """
        name, extras = self.name()
        if " & " in name:
            # no friendly name
            name = ""
        else:
            name = name.replace(u"\xfc", "ue") + "+"*extras + "\n\n"
        return regularFormat % dict(
            name=name,
            melody=regutils.formatMappingMatrix(self.melody),
            reduced=regutils.formatMappingMatrix(
                regutils.reduced_mapping(self.melody)),
            complexity=self.complexity(),
            error=self.optimalError()*1200,
        )

regularFormat = """%(name)smapping by steps:
%(melody)s

reduced mapping:
%(reduced)s

complexity: %(complexity).3f  TOP-RMS error: %(error).3f cent/oct"""


class SanityViolation(Exception):
    """
    Raise when it looks like the search will get out of control
    """

#
#  Test code
#
def test():
    def octaves(ets):
        return [x[0] for x in ets]
    limit5, limit7, limit13, limit19 = [regutils.primes[:n] for n in (3, 4, 6, 8)]
    nonoctave = limit13[1:]
    nofives = [regutils.log2(n) for n in (2, 3, 7, 11, 13)]
    assert len(list(limitedMappings(12, 1e-3, 0.1, limit19))) == 8
    assert len(list(limitedMappings(13, 1e-3, 0.3, nonoctave))) == 35
    assert octaves(getEqualTemperaments(limit7, 0.5/1200, 5)) == [31, 41, 72, 53, 19]
    assert octaves(getEqualTemperaments2(limit7, 0.5/1200, 5)) == [31, 41, 72, 53, 19]
    assert octaves(getEqualTemperaments(nonoctave, 1/120, 5)) == [7, 4, 6, 2, 9]
    assert octaves(getEqualTemperaments2(nonoctave, 1/120, 5)) == [7, 4, 6, 2, 9]
    assert octaves(getEqualTemperaments(nofives, 1/1200, 5)) == [17, 41, 9, 46, 10]
    assert octaves(getEqualTemperaments2(nofives, 1/1200, 5)) == [17, 41, 9, 46, 10]
    assert getEqualTemperaments(limit7, 0.5/1200)[0]==[31, 49, 72, 87]
    assert getEqualTemperaments2(limit7, 0.5/1200)[0]==[31, 49, 72, 87]
    assert octaves(
            getEqualTemperaments(regutils.primes[:15], 0.1/1200, 5)
            ) == [121, 311, 270, 311, 130]
    assert bestET(limit19, 12) == [12, 19, 28, 34, 42, 45, 49, 51]
    syntonic = [[-4, 4, -1, 0]]
    assert bestET(nonoctave, 13) == [13, 19, 23, 28, 30]
    magic = Temperament(limit7, 19, 22)
    assert magic.melody == Temperament('7', 19, 22).melody
    assert magic.plimit == Temperament('7', 19, 22).plimit
    assert octaves(etsForMapping(magic.melody, ek=0.001, nResults=5)) == [19, 41, 22, 60, 3]
    assert etsForMapping(magic.melody, octave=41) == [[41, 65, 95, 115]]
    magic.basis = 0.02690, 0.02226
    assert octaves(etsForTemperament(magic, ek=0.001, nResults=5)) == [19, 41, 22, 60, 3]
    mohajira = Temperament(limit19, 24, 31)
    assert octaves(etsForMapping(mohajira.melody, ek=0.001, nResults=5)) == [31, 24, 7, 55, 38]
    mohajira.basis = 0.01093, 0.02379
    assert octaves(etsForTemperament(mohajira, ek=0.001, nResults=5)) == [31, 24, 7, 55, 38]
    mystery = Temperament(limit19, 29, 58)
    assert octaves(etsForMapping(mystery.melody, ek=0.001, nResults=5)) == [29, 58, 29, 87, 58]
    mystery.basis = 0.03447, 0.01340
    assert octaves(etsForTemperament(mystery, ek=0.001, nResults=5)) == [29, 58, 29, 87, 58]
    assert octaves(uvSurvey1(syntonic, 5, limit7)) == [19, 31, 12, 5, 14]
    assert octaves(uvSurvey1(syntonic, 5, limit13)) == [31, 19, 12, 19, 26]
    assert octaves(uvSurvey1(syntonic, 5, limit19)) == [31, 26, 19, 12, 24]
    marvel = [[-5, 2, 2, -1], [-7, -1, 1, 1, 1, 0]]
    assert octaves(uvSurvey1(marvel, 5, limit13)) == [72, 31, 41, 53, 103]
    assert octaves(uvSurvey1(marvel, 5, limit19)) == [72, 94, 53, 31, 103]
    marvel = Temperament(limit13, 19, 22, 31)
    marvel.basis = 0.01031, 0.01241, 0.01715
    ets, lts, rts = subSurvey(marvel)
    assert octaves(ets) == [72, 31, 53, 103, 19, 50, 22, 125, 41, 84]
    assert [lt.name() for lt in lts[:4]] == [(u'Benediction', 0),
            (u'Catakleismic', 0), (u'Orwell', 0), (u'Lizard', 0)]
    assert rts[0].name() == (u'Marvel', 0)

if __name__=="__main__":
    test()
