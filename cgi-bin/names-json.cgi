#!/usr/bin/env python
import json, names, regutils

print("Cache-Control: public, max-age=14400\r")
print("Content-Type: application/json\r\n\r")

db=[]
for limit, data in names.namesByLimit.items():
    for key, name in data.items():
        for rank in range(1, len(limit) + 1):
            try:
                mapping = regutils.mappingFromInvariant(key, rank)
                if len(mapping[0]) == len(limit):
                    break
            except IndexError:
                pass
        else:
            raise IndexError("Couldn't find mapping " + name)
        db.append(dict(limit=limit, mapping=mapping, name=name))
print(json.dumps(db))
