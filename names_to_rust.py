"""
Convert the names to a Rust source file
"""

from names import namesByLimit

FILE_START = """use lazy_static::lazy_static;
use std::collections::HashMap;

use super::ETMap;

lazy_static! {
    pub static ref NAMES_BY_LIMIT: HashMap<Vec<&'static str>, HashMap<ETMap, &'static str>> =
        HashMap::from([
"""

LIMIT_START = """            (
                vec![{}],
                HashMap::from([
"""

LOOKUP_LINE = '                    (vec![{}], "{}"),\n'

LIMIT_END = """                ]),
            ),
"""

FILE_END = """        ]);
}
"""

output = open("names.rs", "w")

output.write(FILE_START )

for limit, lookup in namesByLimit.items():
    output.write(LIMIT_START.format(', '.join('"{}"'.format(k) for k in limit)))
    for key, name in lookup.items():
        output.write(LOOKUP_LINE.format(
            ', '.join(map(str, key)),
            name.replace('"', '\\"'),
            ))
    output.write(LIMIT_END)

output.write(FILE_END)
